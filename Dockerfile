ARG  JRE8_BASE=adoptopenjdk:8u232-b09-jre-hotspot
FROM ${JRE8_BASE}

ADD apache-tomcat-9.0.30.tar.gz /

RUN rm -rf /apache-tomcat-9.0.30/webapps/* \
    && mkdir -p /archiva-data/conf/

COPY apache-archiva-2.2.4.war /apache-tomcat-9.0.30/webapps/ROOT.war
COPY context.xml /apache-tomcat-9.0.30/conf/
COPY javax.mail.jar /apache-tomcat-9.0.30/lib/

ENV CATALINA_OPTS="-Dappserver.home=/apache-tomcat-9.0.30 -Dappserver.base=/archiva-data"
VOLUME ["/archiva-data"]
EXPOSE 8080

WORKDIR /archiva-data

CMD ["run"]
ENTRYPOINT ["/apache-tomcat-9.0.30/bin/catalina.sh"]
